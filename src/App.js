import React from "react"
import "./App.css"
import { BrowserRouter as Router } from "react-router-dom"
import { Switch, Route } from "react-router-dom"
import Login from "./modules/containers/login/Login"
import Signup from "./modules/containers/signup/Singup"
import { Counter } from "./modules/containers/Counter"

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/counter" component={Counter} />
        </Switch>
      </Router>
    </>
  )
}

export default App
